import pandas as pd
import networkx as nx
import os
import numpy as np
import pickle
import plotly.graph_objects as go
import plotly.express as px
import plotly as py
import streamlit as st

def plotGraphGEO(df,dict_pos,edges):

    fig = go.Figure(data=go.Scattergeo(
            lon = df['Longitude'],
            lat = df['Latitude'],
            mode = 'markers',
            hovertext=list(df.index),
            name='Stazione',
            marker={'size':7},

            ))

    for e in edges:
        fig.add_trace(
            go.Scattergeo(
                lon = [dict_pos[e[0]][0], dict_pos[e[1]][0]],
                lat = [dict_pos[e[0]][1], dict_pos[e[1]][1]],
                mode = 'lines',
                line = dict(width = 1,color = 'red'),
                name=e[0]+'<br> '+e[1],
                showlegend=False
                # opacity = float(df_flight_paths['cnt'][i]) / float(df_flight_paths['cnt'].max()),
            )
        )

    fig.update_layout(
            geo = go.layout.Geo(
                resolution = 50,
                scope = 'europe',
                projection_type = 'mercator',
                showrivers = True,
                rivercolor = '#fff',
                showlakes = True,
                lakecolor = '#fff',
                showland = True,
                showsubunits=True,
                landcolor = '#EAEAAE',
                countrycolor = '#d3d3d3',
                countrywidth = 1.5,
                subunitcolor = '#d3d3d3',
                showframe = False,
                showcoastlines = True,
                coastlinecolor = "#d3d3d3",
                lonaxis_range= [ df['Longitude'].min()-.05, df['Longitude'].max()+.05 ],
                lataxis_range= [ df['Latitude'].min()-.05, df['Latitude'].max()+.05 ]
            ),
        autosize=False,
        showlegend=False,
         height=800,
         width=800
        
        )
    st.plotly_chart(fig, height=800,width=800,mapbox_style="open-street-map",hovermode='closest')
    # fig.show()


def plotGraphStreet(df,dict_pos,edges):

    fig = go.Figure(data=go.Scattermapbox(
            lon = df['Longitude'],
            lat = df['Latitude'],
            mode = 'markers',
            hovertext=list(df.index),
            name='Stazione',
            marker={'size':7},

            ))

    for e in edges:
        fig.add_trace(
            go.Scattermapbox(
                lon = [dict_pos[e[0]][0], dict_pos[e[1]][0]],
                lat = [dict_pos[e[0]][1], dict_pos[e[1]][1]],
                mode = 'lines',
                line = dict(width = 1,color = 'red'),
                name=e[0]+'<br> '+e[1],
                showlegend=False
                # opacity = float(df_flight_paths['cnt'][i]) / float(df_flight_paths['cnt'].max()),
            )
        )

    # fig.update_layout(
    #         mapbox = go.layout.Mapbox(
    #             # resolution = 50,
    #             # scope = 'europe',
    #             # projection_type = 'mercator',
    #             # showrivers = True,
    #             # rivercolor = '#fff',
    #             # showlakes = True,
    #             # lakecolor = '#fff',
    #             # showland = True,
    #             # showsubunits=True,
    #             # landcolor = '#EAEAAE',
    #             # countrycolor = '#d3d3d3',
    #             # countrywidth = 1.5,
    #             # subunitcolor = '#d3d3d3',
    #             # showframe = False,
    #             # showcoastlines = True,
    #             # coastlinecolor = "#d3d3d3",
    #             center=go.layout.mapbox.Center(
    #                         lat= df['Latitude'].mean(),
    #                         lon= df['Longitude'].mean()
    #                         )),
    #     autosize=True,
    #     showlegend=False,
        
        # )
    st.plotly_chart(fig)

def centerByLatLong(X,lat,long,delta=.5):
    new_X0 = {}
    for staz,long_lat in X[0].items():
        if (long_lat[0]>=long-delta)&(long_lat[0]<=long+delta) & (long_lat[1]>=lat-delta)&(long_lat[1]<=lat+delta):
            new_X0[staz] = long_lat
    new_X1 = []
    for stats in X[1]:
        if (stats[0] in set(new_X0.keys())) & (stats[1] in set(new_X0.keys())):
            new_X1.append(stats)
    return new_X0,new_X1

def centerByLatLongMinMax(X,geo_prov):
    min_lat = geo_prov['latitudine'].min()
    max_lat = geo_prov['latitudine'].max()
    min_long = geo_prov['longitudine'].min()
    max_long = geo_prov['longitudine'].max()
    new_X0 = {}
    for staz,long_lat in X[0].items():
        if (long_lat[0]>=min_long-.5)&(long_lat[0]<=max_long+.5) & (long_lat[1]>=min_lat-.5)&(long_lat[1]<=max_lat+.5):
            new_X0[staz] = long_lat
    new_X1 = []
    for stats in X[1]:
        if (stats[0] in set(new_X0.keys())) & (stats[1] in set(new_X0.keys())):
            new_X1.append(stats)
    return new_X0,new_X1



def graphPage():
    X = pickle.load(open('grafo_geolocalizzato_con_linee.svg','rb'))
    geo_prov = pd.read_csv('citta_conosciute.csv',sep=';')

    dict_pos,edges = centerByLatLongMinMax(X,geo_prov)
    st.header('Visualizzazione mappa')
    st.sidebar.markdown("Selezione per provincia")

    prov = st.sidebar.text_input('Provincia')


    if len(prov)>0:
        lat = geo_prov['latitudine'][geo_prov['comuni']==int(prov)].values[0] # TODO: da correggere quando avremo i nomi delle province
        long = geo_prov['longitudine'][geo_prov['comuni']==int(prov)].values[0] # TODO: da correggere quando avremo i nomi delle province
        dict_pos,edges = centerByLatLong(X,lat,long)

    staz = st.sidebar.selectbox('Codice Stazione',['']+list(set(dict_pos.keys())))
    if len(staz)>0:
        lat = dict_pos[staz][1]
        long = dict_pos[staz][0]
        dict_pos,edges = centerByLatLong(X,lat,long,delta=.1)
    
    id_line = st.sidebar.text_input('ID') # TODO: da inserire codice ID
    if len(id_line)>0: 
        df = centerByLine(id_line)
    
    df = pd.DataFrame.from_dict(dict_pos,orient='index',columns=['Longitude','Latitude'])

    plotGraphGEO(df,dict_pos,edges)
